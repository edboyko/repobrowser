//
//  CommitCell.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 16/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import UIKit

class CommitCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var avatarLoadingIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureWithCommit(_ commit: Commit) {
        
        self.avatarImageView?.image = nil // Clear current image from reused cell
        
        self.messageLabel.text = commit.message
        self.loginLabel.text = commit.authorLogin
        
        // Format date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        self.dateLabel.text = dateFormatter.string(from: commit.date)
        if let avatar = commit.avatar {
            self.avatarImageView.image = avatar
        }
        else {
            DataDownloader.downloadImage(imageAddress: commit.authorAvatarAddress) { [weak self] (avatar) in
                DispatchQueue.main.sync {
                    commit.avatar = avatar
                    self?.avatarImageView.image = avatar
                }
            }
        }

        
//        if let avatar = commit.authorAvatar { // Check if avatar image is available for the commit
//            
//            self.avatarImageView.image = avatar
//        }
//        else { // Download avatar if not available
//            
//            // Show loading indicator
//            avatarLoadingIndicator.startAnimating()
//            
//            
//            commit.downloadImage { [weak self] (avatar) in
//                
//                // Access main thread to edit UI
//                DispatchQueue.main.sync {
//                    
//                    // Display freshly downloaded image
//                    self?.avatarImageView.image = avatar
//                    
//                    // Hide loading indicator and stop animation
//                    self?.avatarLoadingIndicator.stopAnimating()
//                }
//            }
//        }
    }
}

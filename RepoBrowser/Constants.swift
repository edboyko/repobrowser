//
//  Constants.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 15/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import Foundation

struct Constants {
    
    static let swiftCommitsLink = "https://api.github.com/repos/apple/swift/commits"
    static let kotlinCommitsLink = "https://api.github.com/repos/jetbrains/kotlin/commits"
    
    static let repos = [
        Repository(name: "Swift", address: "https://api.github.com/repos/apple/swift/commits"),
        Repository(name: "Kotlin", address: "https://api.github.com/repos/jetbrains/kotlin/commits")
    ]
}

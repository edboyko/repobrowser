//
//  File.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 15/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import UIKit

struct CommitInfo: Decodable {
    
    struct Author: Decodable {
        var login: String
        var avatarUrl: String
    }
    struct Commit: Decodable {
        var author: Commit.Author
        var message: String
        struct Author: Decodable {
            var name: String
            var date: Date
        }
    }
    
    var author: Author
    var commit: CommitInfo.Commit
}

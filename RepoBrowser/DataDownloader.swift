//
//  DataDownloader.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 15/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import UIKit

class DataDownloader {

    
    static func downloadCommitData(repoAddress: String, completion: @escaping (([Commit]?, String?) -> Swift.Void)) {
        
        // Check address is valid
        guard let url = URL(string: repoAddress) else {
            return
        }
        
        // Start data download process
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            
            var apiData: Data? = nil
            
            if let httpResponse = response as? HTTPURLResponse { // Check if response is valid
                if httpResponse.statusCode / 100 == 2 { // Check status code is positive
                    
                    if let data = data { // Check data is valid
                        apiData = data
                    }
                }
            }
            
            var failDescription: String?
            var commits: [Commit]?
            if let data = apiData {
                if let deserializedCommits = Deserializer.deserializeCommitData(data) {
                    commits = deserializedCommits
                }
                else {
                    failDescription = "Deserialization Failed"
                }
                
            }
            else {
                
                if let error = error { // Check for error
                    failDescription = error.localizedDescription
                }
                else {
                    failDescription = "Failed to acquire data"
                }
                // Notify delegate that data has not been acquired
                //self?.delegate?.downloader(self!, failedWith: failDescription)
            }
            
            completion(commits, failDescription)
        }
        
        dataTask.resume()
    }
    static func downloadImage(imageAddress: String, finishCallback: @escaping ((UIImage?) -> Swift.Void)) {
        
        if let url = URL(string: imageAddress) { // Check avatar address is valid
            
            // Setup image download process
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
            let task = session.dataTask(with: url) { (data, response, error) in
                
                var avatar: UIImage? = nil
                
                if let error = error { // Check error
                    print("Error downloading Image:", error.localizedDescription)
                }
                else {
                    if let httpResponse = response as? HTTPURLResponse { // Check response is valid
                        if httpResponse.statusCode / 100 == 2 { // Check status code is positive
                            if let data = data {
                                if let image = UIImage(data: data) { // Create image from data
                                    avatar = image
                                    //self.authorAvatar = avatar // Apply downloaded image to the property
                                }
                            }
                        }
                    }
                }
                finishCallback(avatar) // Notify that download is finished
            }
            task.resume()
        }
    }

}

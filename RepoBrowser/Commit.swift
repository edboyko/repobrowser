//
//  Commit.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 21/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import UIKit

class Commit {
    let message: String
    let date: Date
    let authorLogin: String
    let authorName: String
    let authorAvatarAddress: String
    
    var avatar: UIImage?
    
    init(info: CommitInfo) {
        message = info.commit.message
        date = info.commit.author.date
        authorLogin = info.author.login
        authorName = info.commit.author.name
        authorAvatarAddress = info.author.avatarUrl
    }
}

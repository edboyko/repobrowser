//
//  SelectRepoViewController.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 16/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import UIKit

class SelectRepoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Repos"
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension SelectRepoViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.repos.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "RepoCell")
        cell.textLabel?.text = Constants.repos[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Instantiate View Controller
        let commitListVC = storyboard?.instantiateViewController(withIdentifier: "CommitsListViewController") as! CommitsListViewController
        
        // Assign repo case to the "repo" property of the commits list View Controller, in order to display commits from particular repo
        commitListVC.repo = Constants.repos[indexPath.row]
        
        // Show commits list
        self.navigationController?.pushViewController(commitListVC, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

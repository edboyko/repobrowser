//
//  CommitsListViewController.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 15/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import UIKit

class CommitsListViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var repo: Repository!
    
    var commits: [Commit]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize refresh control
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refreshCommitData), for: .valueChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Refresh data
        refreshCommitData()
        tableView.refreshControl?.beginRefreshing()
    }
    
    @objc func refreshCommitData() {
        loadRepo(repo)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    private func loadRepo(_ repo: Repository) {
        
        
        // Edit View Controller Title
        title = repo.name
        
        // Start data downloading
        DataDownloader.downloadCommitData(repoAddress: repo.address) { [weak self] (commits, failDescription) in
            
            self?.commits = commits
            
            DispatchQueue.main.sync {
                self?.tableView.refreshControl?.endRefreshing()
                if let error = failDescription {
                    
                    self?.showAlertWithErrorDescription(error)
                }
                else {
                    self?.tableView.reloadFirstSection()
                }
            }
        }
    }
    
    private func showAlertWithErrorDescription(_ errorDescription: String) {
        let controller = UIAlertController(title: "Error", message: errorDescription, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default)
        
        controller.addAction(okAction)
        
        navigationController?.present(controller, animated: true, completion: nil)
    }
}

extension CommitsListViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let commits = commits else {
            return 0
        }
        return commits.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommitCell", for: indexPath) as? CommitCell, let commits = commits else {
            return UITableViewCell()
        }
        
        cell.configureWithCommit(commits[indexPath.row])
        
        return cell
    }
    
    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
}

//
//  Repo.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 22/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import Foundation

struct Repository {

    var name: String
    var address: String

}

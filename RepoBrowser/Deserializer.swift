//
//  Deserialized.swift
//  RepoBrowser
//
//  Created by Edwin Boyko on 21/08/2018.
//  Copyright © 2018 Edwin Boyko. All rights reserved.
//

import UIKit

class Deserializer {
    
    static func deserializeCommitData(_ data: Data) -> [Commit]? {
        var commits: [Commit]?
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .iso8601
            let result = try decoder.decode([CommitInfo].self, from: data)
            
            commits = [Commit]()
            
            for info in result {
                let commit = Commit(info: info)
                commits?.append(commit)
            }
        }
        catch {
            print("Deserialization Error:", error.localizedDescription) // Print error if failed to deserialize
            
        }
        return commits
    }
    
}
